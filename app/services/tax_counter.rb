class TaxCounter
  attr_accessor :tax_rate

  def initialize(tax_rate)
    @tax_rate = tax_rate
  end

  def calculate_gross(net_cents)
    (net_cents + (net_cents * (tax_rate / 100.0))).round
  end
end

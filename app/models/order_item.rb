class OrderItem < ApplicationRecord
  belongs_to :order

  validates :quantity, presence: true, numericality: { greater_then: 0 }
  validates :net_price_cents, presence: true, numericality: { greater_then: 0 }
  validates :net_total_cents, presence: true, numericality: { greater_then: 0 }
  validates :total_cents, presence: true, numericality: { greater_then: 0 }

  before_validation :set_net_total
  before_validation :set_total
  after_save { order.save }

  private

  def set_net_total
    self.net_total_cents = quantity * net_price_cents
  end

  def set_total
    self.total_cents =
      TaxCounter.new(Order::TAX_RATE).calculate_gross(net_total_cents)
  end
end

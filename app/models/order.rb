class Order < ApplicationRecord
  TAX_RATE = 23

  has_many :order_items, dependent: :destroy

  validates :net_total_cents, presence: true,
    numericality: { greater_than_or_equal_to: 0 }
  validates :total_cents, presence: true,
    numericality: { greater_than_or_equal_to: 0 }
  validates :tax_cents, presence: true,
    numericality: { greater_than_or_equal_to: 0 }

  before_validation :set_net_total
  before_validation :set_total
  before_validation :set_tax

  private

  def set_net_total
    self.net_total_cents = order_items.sum(:net_total_cents)
  end

  def set_total
    self.total_cents =
      TaxCounter.new(TAX_RATE).calculate_gross(net_total_cents)
  end

  def set_tax
    self.tax_cents = total_cents - net_total_cents
  end
end

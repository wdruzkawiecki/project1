class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.integer :net_total_cents, default: 0, null: false
      t.integer :tax_cents, default: 0, null: false
      t.integer :total_cents, default: 0, null: false

      t.timestamps
    end
  end
end

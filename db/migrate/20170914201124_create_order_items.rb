class CreateOrderItems < ActiveRecord::Migration[5.1]
  def change
    create_table :order_items do |t|
      t.belongs_to :order, foreign_key: true
      t.integer :net_price_cents, default: 0, null: false
      t.integer :quantity
      t.integer :net_total_cents, default: 0, null: false
      t.integer :total_cents, default: 0, null: false

      t.timestamps
    end
  end
end

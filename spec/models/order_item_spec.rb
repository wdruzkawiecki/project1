require 'rails_helper'

RSpec.describe OrderItem, type: :model do
  let(:order) { orders(:empty_order) }
  let(:order_item) do
    order.order_items.create(quantity: 3, net_price_cents: 2300)
  end

  context "create order_item" do
    it "sets correct net_total" do
      expect(order_item.net_total_cents).to eq(6900)
    end

    it "counts correct total" do
      expect(order_item.total_cents).to eq(8487)
    end

    it "is persisted" do
      expect(order_item.persisted?).to be_truthy
    end
  end

  context "update order_item" do
    it "counts prices" do
      order_item.update(quantity: 5, net_price_cents: 3250)
      expect(order_item.net_total_cents).to eq(16250)
      expect(order_item.total_cents).to eq(19988)
    end
  end
end

require 'rails_helper'

RSpec.describe Order, type: :model do
  let(:order) { orders(:empty_order) }
  before do
    order.order_items.create(quantity: 3, net_price_cents: 1300)
    order.order_items.create(quantity: 2, net_price_cents: 2710)
  end

  context "create order" do
    it "counts net_total" do
      expect(order.net_total_cents).to eq(9320)
    end

    it "counts total" do
      expect(order.total_cents).to eq(11464)
    end

    it "counts tax" do
      expect(order.tax_cents).to eq(2144)
    end
  end

  context "add new order_item to order" do
    before { order.order_items.create(quantity: 5, net_price_cents: 1234) }

    it "updates net_total" do
      expect(order.net_total_cents).to eq(15490)
    end

    it "updates total" do
      expect(order.total_cents).to eq(19053)
    end

    it "updates tax" do
      expect(order.tax_cents).to eq(3563)
    end
  end
end

require 'rails_helper'

RSpec.describe TaxCounter do
  let(:tax_counter) { TaxCounter.new(23) }

  context "calculate tax" do
    it "calculates net -> gross" do
      expect(tax_counter.calculate_gross(120000)).to eq(147600)
    end

    it "returns 0 when 0 is passed as argument" do
      expect(tax_counter.calculate_gross(0)).to eq(0)
    end

    it "returns provided price when tax_rate is 0" do
      tax_counter.tax_rate = 0
      expect(tax_counter.calculate_gross(71400)).to eq(71400)
    end
  end
end
